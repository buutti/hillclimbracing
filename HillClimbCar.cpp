#include <cmath>
#include <iostream>
#include <memory>
#include "HillClimbCar.h"
#include "HillClimbRoad.h"
#include "HillClimbUtility.h"

namespace hillclimb {
    
    CarWheel::CarWheel(const double x_offset, const double y_offset, const double radius) :
        OFFSET(calculateHypotenuse(x_offset, y_offset)),
        OFFSET_ANGLE(calculateAngleFromComponents(x_offset, y_offset)),
        RADIUS(radius) {
        clearPreviousState();
    }

    double CarWheel::getForceX() {
        return this->f_x;
    }

    double CarWheel::getForceY() {
        return this->f_y;
    }

    std::vector<RoadPartTouching> CarWheel::getRoadPartsTouching() {
        return this->roadPartsTouching;
    }

    void CarWheel::updatePosX(const double carX, const double carAngle) {
        this->x = carX + OFFSET * std::cos(-carAngle + OFFSET_ANGLE);
    }

    void CarWheel::updatePosY(const double carY, const double carAngle) {
        this->y = carY + OFFSET * std::sin(-carAngle + OFFSET_ANGLE);
    }

    void CarWheel::clearPreviousState() {
        this->x = 0.0;
        this->y = 0.0;
        this->f_x = 0.0;
        this->f_y = 0.0;
        this->roadPartsTouching.clear();
    }

    bool CarWheel::touchesRoad() {    
        return this->roadPartsTouching.size() > 0;
    }

    void CarWheel::updateForces(double throttle) {
        if (!this->touchesRoad()) {
            return;
        }

        const double FRICTION = 100.0;
        double slope;
        double slopeAngle;
        double slopeFriction;
        double throttleFriction;
        auto slopeCount = this->roadPartsTouching.size(); 

        if (slopeCount > 0) {
            throttle /= slopeCount;
        }
    
        for (auto roadPart: this->roadPartsTouching) {
            slope = roadPart.slope;
            if (slope >= 0) {
                slopeFriction = FRICTION * (slope + 1);
            } else {
                slopeFriction = FRICTION / (std::abs(slope) + 1);
            }
            slopeAngle = std::atan(slope);
            throttleFriction = throttle - slopeFriction;
            if (throttleFriction > 0) {
                this->f_x += std::cos(slopeAngle) * throttleFriction;
                this->f_y += std::sin(slopeAngle) * throttleFriction;
            }
        }
    }

    void CarWheel::updateRoadPartsTouching(std::shared_ptr<HillClimbRoad> road) {
        double dist;
        double slope;
        Coordinates beginCoords;
        Coordinates endCoords;
    
        const int partCount = road->getPartCount();
        const auto partCoordPairs = road->getPartCoords();
    
        for (int i = 0; i < partCount - 1; i++) {
            beginCoords = partCoordPairs[i];
            endCoords = partCoordPairs[i + 1];

            if (endCoords.x < this->x - this->RADIUS) {
                continue;
            }
            dist = calculateDistanceToLineSegment(this->x, this->y, beginCoords, endCoords);
            if (dist <= this->RADIUS) {
                slope = calculateSlope(beginCoords, endCoords);
                auto roadPart = RoadPartTouching{ .wheelSubmersion = this->RADIUS - dist, .slope = slope };
                this->roadPartsTouching.push_back(roadPart);
            }
            if (beginCoords.x > this->x + this->RADIUS) {
                break;
            }
        }
    }

    void CarWheel::updateState(const double throttle, const Coordinates carPos, const double carAngle,
                               std::shared_ptr<HillClimbRoad> road) {
        this->updatePosX(carPos.x, carAngle);
        this->updatePosY(carPos.y, carAngle);
        this->updateRoadPartsTouching(road);
        this->updateForces(throttle);
    }

    HillClimbCar::HillClimbCar(const double x, const double y, const double scale) : X_POS(x), SCALE(scale) {
        const double LEFT_WHEEL_X_OFFSET = -57.0;
        const double RIGHT_WHEEL_X_OFFSET = 58.0;
        const double WHEELS_Y_OFFSET = -43.0;
        const double WHEEL_RADIUS = 18.0;

        this->y = y;
        this->v_x = 0.0;
        this->v_y = 0.0;
        this->a_x = 0.0;
        this->a_y = 0.0;
        this->angle = 0.0;
        this->v_ang = 0.0;
        this->a_ang = 0.0;
        this->throttle = 0.0;

        this->leftWheel = std::shared_ptr<CarWheel>(new CarWheel(LEFT_WHEEL_X_OFFSET * SCALE,
                                                                 WHEELS_Y_OFFSET * SCALE,
                                                                 WHEEL_RADIUS * SCALE));
        this->rightWheel = std::shared_ptr<CarWheel>(new CarWheel(RIGHT_WHEEL_X_OFFSET * SCALE,
                                                                  WHEELS_Y_OFFSET * SCALE,
                                                                  WHEEL_RADIUS * SCALE));
    }

    void HillClimbCar::updateVelocityX(double dt) {
        this->v_x = this->v_x + this->a_x * dt;
    }

    void HillClimbCar::updatePosY(double dt) {
        double slope;
        double wheelSubmersion;
        double normalAngle;
        double neededFix;
        double fixed = 0;

        for (auto roadPart: this->roadPartsTouching) {
            slope = roadPart.slope;
            wheelSubmersion = roadPart.wheelSubmersion;
            normalAngle = calculateNormalAngleFromSlope(slope);
            neededFix = std::abs(wheelSubmersion * std::sin(normalAngle));
            if (fixed < neededFix) {
                fixed = neededFix;
            }
        }
        this->y += fixed;
        this->y = this->y + this->v_y * dt + this->a_y * std::pow(dt, 2);
    }

    void HillClimbCar::updateVelocityY(double dt) {
        this->v_y = this->v_y + this->a_y * dt;
    }

    void HillClimbCar::updateAngle(double dt) {
        this->angle = std::fmod(this->angle + this->v_ang * dt + this->a_ang * std::pow(dt, 2), FULL_CIRCLE);
    }

    void HillClimbCar::updateAngularVelocity(double dt) {
        const double V_ANG_MAX = 0.7;
        this->v_ang = this->v_ang + this->a_ang * dt;
        if (this->v_ang > V_ANG_MAX) {
            this->v_ang = V_ANG_MAX;
        } else if (this->v_ang < -V_ANG_MAX) {
            this->v_ang = -V_ANG_MAX;
        }
    }

    void HillClimbCar::updateAngularAcceleration(double dt) {
        const double PARALLEL_WITH_ROAD = 0.02;
        const double ALMOST_PARALLEL_WITH_ROAD = 0.05;
        const double DIFF_FACTOR = 200;
        const double ANGULAR_ACCELERATION = 3.0;
        const double STEEP_SLOPE = RIGHT_ANGLE / 4;

        double slope;
        double slopeAngle;
        double angleDiff;
        double angleDiffAbs;
    
        for (auto roadPart: this->roadPartsTouching) {
            slope = roadPart.slope;
            slopeAngle = atan(slope);
            angleDiff = calculateAngleDiff(slopeAngle, -this->angle);
            angleDiffAbs = std::abs(angleDiff);

            if (angleDiffAbs <= PARALLEL_WITH_ROAD) {
                this->a_ang = -this->v_ang / dt;
            } else if (angleDiffAbs <= ALMOST_PARALLEL_WITH_ROAD) {
                this->a_ang = -this->v_ang / dt - angleDiff * DIFF_FACTOR;
            } else if (angleDiffAbs <= RIGHT_ANGLE / 2 || slope < -STEEP_SLOPE || slope > STEEP_SLOPE) {
                this->a_ang = -angleDiff * ANGULAR_ACCELERATION;
            }
        }
        this->updateAngularVelocity(dt);
        this->updateAngle(dt);
    }

    void HillClimbCar::updateRoadPartsTouching() {
        std::vector<RoadPartTouching> rightWheelRoadPartsTouching;
    
        this->roadPartsTouching.clear();
        this->roadPartsTouching = this->leftWheel->getRoadPartsTouching();
        rightWheelRoadPartsTouching = this->rightWheel->getRoadPartsTouching();

        this->roadPartsTouching.insert(this->roadPartsTouching.end(), rightWheelRoadPartsTouching.begin(),
                                       rightWheelRoadPartsTouching.end());
    }

    void HillClimbCar::updateWheels(std::shared_ptr<HillClimbRoad> road) {
        const Coordinates carPos = {
            .x = this->X_POS,
            .y = this->y
        };

        this->leftWheel->clearPreviousState();
        this->rightWheel->clearPreviousState();
        this->leftWheel->updateState(this->throttle, carPos, this->angle, road);
        this->rightWheel->updateState(this->throttle, carPos, this->angle, road);
    }

    void HillClimbCar::updateAccelerations(double dt) {
        const double GRAVITY = 200.0;
    
        double slope;
        double normalAngle;
        double projectionForce;
        double projectionVelocity;
        double supportForce;
    
        double residualForce;
        double residualVelocity;
        double residualForceAngle;
        double residualVelocityAngle;
        
        double residualVelocityX = this->v_x;
        double residualVelocityY = this->v_y;
        double residualForceX = this->leftWheel->getForceX() + this->rightWheel->getForceX();
        double residualForceY = this->leftWheel->getForceY() + this->rightWheel->getForceY() - GRAVITY;

        for (auto roadPart: this->roadPartsTouching) {
            slope = roadPart.slope;
            normalAngle = calculateNormalAngleFromSlope(slope);
            residualForce = calculateHypotenuse(residualForceX, residualForceY);
            residualVelocity = calculateHypotenuse(residualVelocityX, residualVelocityY);
            residualForceAngle = calculateAngleFromComponents(residualForceX, residualForceY);
            residualVelocityAngle  = calculateAngleFromComponents(residualVelocityX, residualVelocityY);        
            projectionForce = calculateProjection(residualForce, residualForceAngle, normalAngle);
            projectionVelocity = calculateProjection(residualVelocity, residualVelocityAngle, normalAngle);
            if (projectionVelocity + projectionForce * dt > 0) {
                supportForce = projectionVelocity / dt + projectionForce;
                residualForceX -= supportForce * cos(normalAngle);
                residualForceY -= supportForce * sin(normalAngle);
                residualVelocityX += residualForceX * dt;
                residualVelocityY += residualForceY * dt;
            }
        }
        this->a_x = residualForceX;
        this->a_y = residualForceY;
    }

    void HillClimbCar::updateThrottle(const double dThrottle) {
        const double MAX_THROTTLE = 300.0;
        const double MIN_THROTTLE = -100.0;
        if ((dThrottle > 0 && this->throttle + dThrottle < MAX_THROTTLE) ||
            (dThrottle < 0 && this->throttle - dThrottle > MIN_THROTTLE)) {
            this->throttle += dThrottle;
        }
    }

    double HillClimbCar::getTransitionX(double dt) {
        return this->v_x * dt + this->a_x * pow(dt, 2);
    }

    double HillClimbCar::getPositionY() {
        return this->y;
    }

    double HillClimbCar::getAngle() {
        return this->angle;
    }

    bool HillClimbCar::touchesRoad() {
        return this->roadPartsTouching.size() > 0;
    }
        
    void HillClimbCar::update(std::shared_ptr<HillClimbRoad> road, double dt) {
        this->updateRoadPartsTouching();
        this->updateWheels(road);
        this->updateAngularAcceleration(dt);
        this->updateAccelerations(dt);
        this->updateVelocityX(dt);
        this->updateVelocityY(dt);
        this->updatePosY(dt);
    }

    void HillClimbCar::reset(const double y) {
        this->y = y;
        this->v_x = 0.0;
        this->v_y = 0.0;
        this->a_x = 0.0;
        this->a_y = 0.0;
        this->angle = 0.0;
        this->v_ang = 0.0;
        this->a_ang = 0.0;
        this->throttle = 0.0;
        this->leftWheel->clearPreviousState();
        this->rightWheel->clearPreviousState();
    }
}
