#include <iostream>
#include <cmath>

#include "cocos2d.h"
#include "HillClimbLayer.h"
#include "HillClimbRoad.h"
#include "HillClimbUtility.h"

namespace hillclimb {

    std::map<cocos2d::EventKeyboard::KeyCode,
             std::chrono::high_resolution_clock::time_point> HillClimbLayer::keys;

    bool HillClimbLayer::init() {
        if (!Layer::init()) {
            return false;
        }

        const auto director = cocos2d::Director::getInstance();
        const auto winSize = director->getWinSize();
        double winWidth = static_cast<double>(winSize.width);
        double winHeight = static_cast<double>(winSize.height);
    
        double spriteScale = DESIGN_RESOLUTION_SIZE.width / winWidth;
        this->carSprite = cocos2d::Sprite::create("car.png");
        this->carSprite->setScale(spriteScale, spriteScale);

        const auto carSize = this->carSprite->getContentSize();
        double carStartX = carSize.width / 2;
        this->carStartY = winSize.height / 2.0 + carSize.height;
        this->carSprite->setPosition(carStartX, carStartY);
        this->carSprite->setAnchorPoint(cocos2d::Vec2(0.5, 0.5));
        this->addChild(this->carSprite, 0);
    
        this->car = std::shared_ptr<HillClimbCar>(new HillClimbCar(carStartX, carStartY, spriteScale));
        this->road = std::shared_ptr<HillClimbRoad>(new HillClimbRoad(winWidth, winHeight));
        this->generateRoadParts();

        auto eventListener = cocos2d::EventListenerKeyboard::create();
        director->getOpenGLView()->setIMEKeyboardState(true);
        eventListener->onKeyPressed = [=](cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event){
            if (keys.find(keyCode) == keys.end()){
                keys[keyCode] = std::chrono::high_resolution_clock::now();
            }
        };
        eventListener->onKeyReleased = [=](cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event){
            keys.erase(keyCode);
        };
        this->_eventDispatcher->addEventListenerWithSceneGraphPriority(eventListener, this);
        this->scheduleUpdate();
    
        return true;
    }

    bool HillClimbLayer::isKeyPressed(cocos2d::EventKeyboard::KeyCode code) {
        if (keys.find(code) != keys.end())
            return true;
        return false;
    }

    void HillClimbLayer::generateRoadParts() {
        const int MIN_ROAD_SIZE = 2;
        const int partCount = this->road->getPartCount();
        const auto partCoordPairs = this->road->getPartCoords();

        Coordinates beginCoords;
        Coordinates endCoords;
    
        if (partCount < MIN_ROAD_SIZE) {
            return;
        }

        auto drawNode = cocos2d::DrawNode::create();
        drawNode->setName("drawNode");
        for (int i = 0; i < partCount - 1; i++) {
            beginCoords = partCoordPairs[i];
            endCoords = partCoordPairs[i + 1];
            drawNode->drawLine(cocos2d::Point(beginCoords.x, beginCoords.y),
                               cocos2d::Point(endCoords.x, endCoords.y),
                               cocos2d::Color4F::WHITE);
        }
        this->addChild(drawNode);
    }

    void HillClimbLayer::deleteRoadParts() {
        this->removeChildByName("drawNode");
    }

    void HillClimbLayer::update(float dt) {
        const double CRASH_MARGIN = RIGHT_ANGLE / 3;
        const double REVERSE_MARGIN = 50;
    
        double carAngle;
        double carTransition;
        bool carCrashed;
    
        cocos2d::Node::update(dt);
        if (isKeyPressed(cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW)) {
            this->car->updateThrottle(5);
        } else if (isKeyPressed(cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW)) {
            this->car->updateThrottle(-10);
        } else {
            this->car->updateThrottle(-5);
        }

        carTransition = this->car->getTransitionX(static_cast<double>(dt));
        carAngle = std::fmod(this->car->getAngle(), STRAIGHT_ANGLE);
    
        carCrashed = ((carAngle > RIGHT_ANGLE + CRASH_MARGIN ||
                       carAngle < -RIGHT_ANGLE - CRASH_MARGIN) &&
                      this->car->touchesRoad());
        if (carTransition / dt < -REVERSE_MARGIN || carCrashed) {
            this->car->reset(this->carStartY);
            this->road->reset();
        } else {
            this->car->update(road, static_cast<double>(dt));
            this->road->move(carTransition);
        }
        this->deleteRoadParts();
        this->generateRoadParts();
        this->carSprite->setPositionY(this->car->getPositionY());
        this->carSprite->setRotation(carAngle / STRAIGHT_ANGLE * 180);
    }

    cocos2d::Scene* HillClimbLayer::createScene() {
        auto scene = cocos2d::Scene::create();
        auto layer = HillClimbLayer::create();

        scene->addChild(layer);
        return scene;
    }
}
