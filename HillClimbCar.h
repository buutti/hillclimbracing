#ifndef __HILLCLIMB_CAR_H__
#define __HILLCLIMB_CAR_H__

#include <vector>
#include <memory>
#include "HillClimbRoad.h"

namespace hillclimb {
    struct RoadPartTouching {
        double wheelSubmersion;
        double slope;
    };

    class CarWheel {
    public:
        CarWheel(const double x_offset, const double y_offset, const double radius);
        double getForceX();
        double getForceY();
        std::vector<RoadPartTouching> getRoadPartsTouching();
        void updateState(const double throttle, const Coordinates carPos, const double CarAngle,
                         std::shared_ptr<HillClimbRoad> road);
        void clearPreviousState();
    private:
        const double OFFSET;
        const double OFFSET_ANGLE;
        const double RADIUS;

        double x;
        double y;
        double f_x;
        double f_y;
        std::vector<RoadPartTouching> roadPartsTouching;
    
        bool touchesRoad();
        void updatePosX(const double carX, const double carAngle);
        void updatePosY(const double carY, const double carAngle);
        void updateRoadPartsTouching(std::shared_ptr<HillClimbRoad> road);
        void updateForces(double throttle);
    };

    class HillClimbCar {
    public:
        HillClimbCar(const double x, const double y, const double scale);
        void update(std::shared_ptr<HillClimbRoad> road, double dt);
        void updateThrottle(const double dthrottle);
        double getPositionY();
        double getTransitionX(double dt);
        double getAngle();
        bool touchesRoad();
        void reset(const double y);
    private:
        const double X_POS;
        const double SCALE;
        double y;
        double v_x;
        double v_y;
        double a_x;
        double a_y;
        double angle;
        double v_ang;
        double a_ang;
        double throttle;
        std::shared_ptr<CarWheel> leftWheel;
        std::shared_ptr<CarWheel> rightWheel;
        std::vector<RoadPartTouching> roadPartsTouching;
    
        void updatePosY(double dt);
        void updateVelocityY(double dt);
        void updateVelocityX(double dt);
        void updateAngle(double dt);
        void updateAngularVelocity(double dt);
        void updateRoadPartsTouching();
        void updateAngularAcceleration(double dt);
        void updateWheels(std::shared_ptr<HillClimbRoad> road);
        void updateAccelerations(double dt);
    };
}

#endif
