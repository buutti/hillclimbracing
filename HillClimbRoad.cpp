#include <cstdlib>
#include <iostream>
#include <ctime>
#include <vector>
#include "HillClimbRoad.h"

namespace hillclimb {
    const int ROAD_LENGTH_FACTOR = 5;

    HillClimbRoad::HillClimbRoad(const int winWidth, const int winHeight) : X_ROAD_START(0),
                                                                            Y_ROAD_START(winHeight / 2),
                                                                            MAX_PART_COUNT(10),
                                                                            DEFAULT_ROAD_LENGTH(winWidth / ROAD_LENGTH_FACTOR) {
        this->reset();
    }

    int HillClimbRoad::getPartCount() {
        return static_cast<int>(this->partCoords.size());
    }

    std::vector<Coordinates> HillClimbRoad::getPartCoords() {
        return this->partCoords;
    }

    void HillClimbRoad::addPart(double x, double y) {
        Coordinates partCoord = {
            .x = x,
            .y = y
        };
        this->partCoords.push_back(partCoord);
    }

    double HillClimbRoad::calculateNewPartX(double prevPartX) {
        return prevPartX + DEFAULT_ROAD_LENGTH + std::rand() % (DEFAULT_ROAD_LENGTH * 3 / 2);
    }

    double HillClimbRoad::calculateNewPartY() {
        return std::rand() % Y_ROAD_START + Y_ROAD_START / 2;
    }

    void HillClimbRoad::generatePartsAhead() {
        double partX;
        double partY;
        int currentPartCount = this->getPartCount();
    
        if (currentPartCount >= this->MAX_PART_COUNT) {
            return;
        }

        if (currentPartCount == 0) {
            partX = this->X_ROAD_START;
            partY = this->Y_ROAD_START;
        } else {
            auto iterLastPart = this->partCoords.back();
            partX = calculateNewPartX(iterLastPart.x);
            partY = calculateNewPartY();
        }

        for (int i = 0; i < this->MAX_PART_COUNT - currentPartCount; i++) {
            this->addPart(partX, partY);
            partX = calculateNewPartX(partX);
            partY = calculateNewPartY();
        }
    }

    void HillClimbRoad::deletePartsBehind() {
        auto iter = this->partCoords.begin();
        ++iter;

        while (iter != this->partCoords.end()) {
            if (iter->x < -DEFAULT_ROAD_LENGTH * ROAD_LENGTH_FACTOR) {
                this->partCoords.erase(std::prev(iter));
            } else {
                ++iter;
            }
        }
    }

    void HillClimbRoad::move(double x) {
        for (auto &coords: this->partCoords) {
            coords.x -= x;
        }

        this->deletePartsBehind();
        this->generatePartsAhead();
    }

    void HillClimbRoad::reset() {
        this->partCoords.clear();
        this->addPart(X_ROAD_START, Y_ROAD_START);
        this->addPart(X_ROAD_START + DEFAULT_ROAD_LENGTH, Y_ROAD_START);
        this->generatePartsAhead();
    }
}
