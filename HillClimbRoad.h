#ifndef __HILLCLIMB_ROAD_H__
#define __HILLCLIMB_ROAD_H__

#include <vector>
#include <HillClimbUtility.h>

namespace hillclimb {

    class HillClimbRoad {
    public:
        const int MAX_PART_COUNT;
        const int X_ROAD_START;
        const int Y_ROAD_START;
        const int DEFAULT_ROAD_LENGTH;

        HillClimbRoad(const int winWidth, const int winHeight);
        void move(double x);
        std::vector<Coordinates> getPartCoords();
        int getPartCount();
        void reset();
    private:
        std::vector<Coordinates> partCoords;
        void addPart(double x, double y);
        double calculateNewPartX(double prevPartX);
        double calculateNewPartY();
        void generatePartsAhead();
        void deletePartsBehind();
    };
}

#endif
